//
//  SellCream.swift
//  VendedorApp
//
//  Created by students@deti on 23/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//


import UIKit
import AVFoundation

class SellCream: UICollectionViewController {
    var gelados:[Gelado] = []
    var tempGelados:[IceCream] = []
    let nc = NSNotificationCenter.defaultCenter()
    @IBOutlet var myCollection: UICollectionView!
    var icecreams:[IceCream]=[
                            IceCream(nome: "Cornetto Chocolate",quant: 0,preco: 1.5,img: "corneto_chocolate"),
                              IceCream(nome: "Cornetto Morango",quant: 0,preco: 1.55,img: "corneto_morango"),
                              IceCream(nome: "Cornetto Limão",quant: 0,preco: 1.7,img: "corneto_limao"),
                              IceCream(nome: "Cornetto Nata",quant: 0,preco: 1.8,img: "corneto_nata"),
                              IceCream(nome: "Fizz Limão",quant: 0,preco: 1.9,img: "fizz_limao"),
                              IceCream(nome: "Magnum Amendoa",quant: 0,preco: 2.5,img: "magnum_amendoa"),
                              IceCream(nome: "Magnum Branco",quant: 0,preco: 5,img: "magnum_branco"),
                              IceCream(nome: "Magnum classico",quant: 0,preco: 2,img: "magnum_classico"),
                              IceCream(nome: "Perna de Pau",quant: 0,preco: 1.5,img: "perna_de_pau"),
                              IceCream(nome: "Solero",quant: 0,preco: 3,img: "solero"),
                              IceCream(nome: "Super Maxi",quant: 0,preco: 1.75,img: "super_maxi"),
    ]
    let identifier = "cellIceream"
    //let regionRadius: CLLocationDistance = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nc.addObserver(self, selector: #selector(saveBuy), name: "saveBuy", object: nil)
        nc.addObserver(self, selector: #selector(resetValues), name: "resetValues", object: nil)
        
        
        
        gelados = loadGelados()!
        
        syncStock()
        
        if let layout = collectionView?.collectionViewLayout as? SellLayout {
            layout.delegate = self
        }
        
        //collectionView.dataSource = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    func syncStock()
    {
        if gelados.count>0
        {
            
            var name:String
            for index in 0...gelados.count-1
            {
                //print("init")
                name = (gelados[index].name as? String)!
                for indexTmp in 0...icecreams.count-1
                {
                    //print(icecreams[indexTmp].nome! + " e " + name)
                    if (icecreams[indexTmp].nome?.caseInsensitiveCompare(name) == NSComparisonResult.OrderedSame)
                    {
                        //print("fim")
                        icecreams[indexTmp].quant = gelados[index].quant
                    }
                }
                
                
            }
        }
        tempGelados = icecreams
        self.myCollection.reloadData()
    }
    func syncStockToSave()
    {
        if gelados.count>0
        {
            
            var name:String
            for index in 0...gelados.count-1
            {
                
                name = (gelados[index].name as? String)!
                for indexTmp in 0...icecreams.count-1
                {
                    //print(icecreams[indexTmp].nome! + " e " + name)
                    if (icecreams[indexTmp].nome?.caseInsensitiveCompare(name) == NSComparisonResult.OrderedSame)
                    {
                        if(gelados[index].quant != icecreams[indexTmp].quant!)
                        {
                            //print(gelados[index].name)
                            var total = tempGelados[indexTmp].quant! - icecreams[indexTmp].quant!
                            print(tempGelados[indexTmp].quant!)
                            print(icecreams[indexTmp].quant!)
                            gelados[index].vendas = total//icecreams[indexTmp].quant! - //gelados[index].vendas + 1
                            //print( gelados[index].vendas)
                        }
                         gelados[index].quant = icecreams[indexTmp].quant!
                    }
                }
                
                
            }
        }
        tempGelados = icecreams
        saveStock()
    }
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return icecreams.count
    }
    
    func saveStock()
    {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(gelados, toFile: Gelado.ArchiveURL.path!)
        if !isSuccessfulSave {
            print("Failed to save Gelados...")
        }
    }
    func loadGelados() -> [Gelado]? {
        return NSKeyedUnarchiver.unarchiveObjectWithFile(Gelado.ArchiveURL.path!) as? [Gelado]
    }
    
    
    //3
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellIcecream", forIndexPath: indexPath) as! IceCreamCell
        //cell.backgroundColor = UIColor.blackColor()
        // Configure the cell
        //2
        //let flickrPhoto = photoForIndexPath(indexPath)
        let icecream = icecreams[indexPath.row]
        cell.precoLabel.text = "\(icecream.preco!)€"
        cell.quantLabel.text = "\(icecream.quant!) Unidades"
        //3
        cell.imageview.image = UIImage(named:icecream.img!)
        return cell
    }


}
extension SellCream : SellLayoutDelegate {
    // 1
    func collectionView(collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath: NSIndexPath,
                        withWidth width: CGFloat) -> CGFloat {
        let photo = icecreams[indexPath.item]
        let boundingRect =  CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
        let rect  = AVMakeRectWithAspectRatioInsideRect(UIImage(named: photo.img!)!.size, boundingRect)
        return rect.size.height
    }
    
    // 2
    func collectionView(collectionView: UICollectionView,
                        heightForAnnotationAtIndexPath indexPath: NSIndexPath, withWidth width: CGFloat) -> CGFloat {
        let annotationPadding = CGFloat(4)
        let annotationHeaderHeight = CGFloat(17)
        let photo = icecreams[indexPath.item]
        let font = UIFont(name: "AvenirNext-Regular", size: 10)!
        //let commentHeight = photo.nome(font, width: width)
        let height = annotationPadding + annotationHeaderHeight + /*commentHeight +*/ annotationPadding
        return height
    }
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        //let url = thumbnailFileURLS[indexPath.item]
        //if UIApplication.sharedApplication().canOpenURL(url) {
          //  UIApplication.sharedApplication().openURL(url)
        //}
        let value = icecreams[indexPath.item]
        if(value.quant>0)
        {
            
            icecreams[indexPath.item].quant = icecreams[indexPath.item].quant! - 1
            myCollection.reloadData()
            let preco = value.preco
            let pos = indexPath.item
            let myDict:NSDictionary = ["preco":preco!, "pos":pos]
            let nc = NSNotificationCenter.defaultCenter()
            nc.postNotificationName("UserLoggedIn", object: myDict  )
        }
        else
        {
            if #available(iOS 8.0, *) {
                let alertController = UIAlertController(title: "Sem stock", message:
                    "O gelado selecionado não existe em stock ", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                let alert = UIAlertView()
                alert.title = "Sem stock"
                alert.message = "O gelado selecionado não existe em stock"
                alert.addButtonWithTitle("Ok")
                alert.show()
                // Fallback on earlier versions
            }
            
        }
    }
    func resetValues()
    {
        //print("entrei1")
        icecreams = tempGelados
        myCollection.reloadData()
    }
    func saveBuy()
    {
        //print("entrei2")
        
        syncStockToSave()
        
    }
}
