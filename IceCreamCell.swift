//
//  IceCreamCell.swift
//  VendedorApp
//
//  Created by students@deti on 23/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import UIKit

class IceCreamCell: UICollectionViewCell {

    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var precoLabel: UILabel!
    @IBOutlet weak var quantLabel: UILabel!
    override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
        super.applyLayoutAttributes(layoutAttributes)
        /*if let attributes = layoutAttributes as? SellLayoutAttributes {
            imageViewHeightLayoutConstraint.constant = attributes.photoHeight
        }*/
    }
}
