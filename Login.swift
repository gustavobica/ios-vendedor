//
//  Login.swift
//  VendedorApp
//
//  Created by students@deti on 24/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import UIKit

class Login: UIViewController,UITextFieldDelegate  {
    @IBOutlet weak var txtValue: UITextField!
    var URL = "http:www.gustavobica.com/CM/API.php?action=nearCar"
    var carrosID:[Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        txtValue.delegate = self //set delegate to textfile
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        loadDataFromURL(URL)
        
        /*print(loadMeals()![0].name)
        print(loadMeals()![0].quant)
        print(loadMeals()?.count)*/
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        //print(textField.text)
        //print("global class is " + mainInstance.name)
        if  carrosID.contains(Int(textField.text!)!)
        {
            mainInstance.id = Int(textField.text!)!
            performSegueWithIdentifier("login", sender: nil)
            return true
            
        }
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "ID inválido", message:
                "O ID inserido não é valido!! ", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alert = UIAlertView()
            alert.title = "ID inválido"
            alert.message = "O ID inserido não é valido!!"
            alert.addButtonWithTitle("Ok")
            alert.show()
            // Fallback on earlier versions
        }
       
        
        return true
    }
    
    func loadDataFromURL(url: String)
    {
        
        
        // Asynchronous Http call to your api url, using NSURLSession:
        NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: url)!, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            if error == nil && data != nil
            {
                //print(data)
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                    let json =
                        try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? Array<AnyObject>//[String:AnyObject]
                    // Access specific key with value of type String
                   
                    self.carrosID.removeAll()
                    var id:String
                    
                    //print(json)
                    for index in 0...json!.count-1
                    {
                        //print(json![index])
                        var id = json![index]["carId"] as? Int
                        //id = id["carId"] as? String
                        self.carrosID.append(id!)
                    }

                    
                } catch {
                    //print("aqui")
                    //print(data)
                    // Something went wrong
                }
            }
        }).resume()
    }
    
}
