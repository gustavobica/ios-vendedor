//
//  GraphTop.swift
//  VendedorApp
//
//  Created by students@deti on 26/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import UIKit

@IBDesignable class GraphTop: UIView {
    
    //1 - the properties for the gradient
    @IBInspectable var startColor: UIColor = UIColor.redColor()
    @IBInspectable var endColor: UIColor = UIColor.greenColor()
    
    @IBOutlet weak var img: UILabel!
    @IBOutlet weak var img1: UILabel!
    @IBOutlet weak var img2: UILabel!
    @IBOutlet weak var img3: UILabel!
    @IBOutlet weak var img4: UILabel!
    @IBOutlet weak var img5: UILabel!
    @IBOutlet weak var img6: UILabel!
    @IBOutlet weak var img7: UILabel!
    @IBOutlet weak var img8: UILabel!
    @IBOutlet weak var img9: UILabel!
    @IBOutlet weak var img10: UILabel!
    
    var gelados:[Gelado] = []
    var icecreams:[IceCream]=[
        IceCream(nome: "Cornetto Chocolate",quant: 0,preco: 1.5,img: "corneto_chocolate"),
        IceCream(nome: "Cornetto Morango",quant: 0,preco: 1.55,img: "corneto_morango"),
        IceCream(nome: "Cornetto Limão",quant: 0,preco: 1.7,img: "corneto_limao"),
        IceCream(nome: "Cornetto Nata",quant: 0,preco: 1.8,img: "corneto_nata"),
        IceCream(nome: "Fizz Limão",quant: 0,preco: 1.9,img: "fizz_limao"),
        IceCream(nome: "Magnum Amendoa",quant: 0,preco: 2.5,img: "magnum_amendoa"),
        IceCream(nome: "Magnum Branco",quant: 0,preco: 5,img: "magnum_branco"),
        IceCream(nome: "Magnum classico",quant: 0,preco: 2,img: "magnum_classico"),
        IceCream(nome: "Perna de Pau",quant: 0,preco: 1.5,img: "perna_de_pau"),
        IceCream(nome: "Solero",quant: 0,preco: 3,img: "solero"),
        IceCream(nome: "Super Maxi",quant: 0,preco: 1.75,img: "super_maxi"),
        ]
    
    var labelsNum:[UILabel] = []
    
    
    var graphPoints:[Int] = [0,0,0,0,0,0,0,0,0,0,0]
    
    func loadGelados() -> [Gelado]? {
        return NSKeyedUnarchiver.unarchiveObjectWithFile(Gelado.ArchiveURL.path!) as? [Gelado]
    }
    override func drawRect(rect: CGRect) {
        labelsNum.removeAll()
        labelsNum.append(img)
        labelsNum.append(img1)
        labelsNum.append(img2)
        labelsNum.append(img3)
        labelsNum.append(img4)
        labelsNum.append(img5)
        labelsNum.append(img6)
        labelsNum.append(img7)
        labelsNum.append(img8)
        labelsNum.append(img9)
        labelsNum.append(img10)
        
        //graphPoints.removeAll()
        gelados = loadGelados()!
        if gelados.count != 0
        {
            syncStock()
            
        }
        
        
        
        
        
        let width = rect.width
        let height = rect.height
        
        //set up background clipping area
        var path = UIBezierPath(roundedRect: rect,
                                byRoundingCorners: UIRectCorner.AllCorners,
                                cornerRadii: CGSize(width: 8.0, height: 8.0))
        path.addClip()
        //2 - get the current context
        let context = UIGraphicsGetCurrentContext()
        let colors = [startColor.CGColor, endColor.CGColor]
        
        //3 - set up the color space
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        //4 - set up the color stops
        let colorLocations:[CGFloat] = [0.0, 1.0]
        
        //5 - create the gradient
        let gradient = CGGradientCreateWithColors(colorSpace,
                                                  colors,
                                                  colorLocations)
        
        //6 - draw the gradient
        var startPoint = CGPoint.zero
        var endPoint = CGPoint(x:0, y:self.bounds.height)
        CGContextDrawLinearGradient(context,
                                    gradient,
                                    startPoint,
                                    endPoint,
                                    [])
        let margin:CGFloat = 20.0
        var columnXPoint = { (column:Int) -> CGFloat in
            //Calculate gap between points
            let spacer = (width - margin*2 - 4) /
                CGFloat((self.graphPoints.count - 1))
            var x:CGFloat = CGFloat(column) * spacer
            x += margin + 2
            return x
        }
        let topBorder:CGFloat = 60
        let bottomBorder:CGFloat = 50
        let graphHeight = height - topBorder - bottomBorder
        let maxValue = graphPoints.maxElement()
        var columnYPoint = { (graphPoint:Int) -> CGFloat in
            var y:CGFloat = CGFloat(graphPoint) /
                CGFloat(maxValue!) * graphHeight
            y = graphHeight + topBorder - y // Flip the graph
            return y
        }
        // draw the line graph
        
        UIColor.whiteColor().setFill()
        UIColor.whiteColor().setStroke()
        
        //set up the points line
        var graphPath = UIBezierPath()
        //go to start of line
        graphPath.moveToPoint(CGPoint(x:columnXPoint(0),
            y:columnYPoint(graphPoints[0])))
        
        //add points for each item in the graphPoints array
        //at the correct (x, y) for the point
        for i in 1..<graphPoints.count {
            let nextPoint = CGPoint(x:columnXPoint(i),
                                    y:columnYPoint(graphPoints[i]))
            graphPath.addLineToPoint(nextPoint)
        }
        let highestYPoint = columnYPoint(maxValue!)
        startPoint = CGPoint(x:margin, y: highestYPoint)
        endPoint = CGPoint(x:margin, y:self.bounds.height)
        
        CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, [])
        //CGContextRestoreGState(context)
        //graphPath.stroke()
        //draw the line on top of the clipped gradient
        graphPath.lineWidth = 2.0
        graphPath.stroke()
        //Draw the circles on top of graph stroke
        for i in 0..<graphPoints.count {
            var point = CGPoint(x:columnXPoint(i), y:columnYPoint(graphPoints[i]))
            point.x -= 5.0/2
            point.y -= 5.0/2
            
            
            
            let circle = UIBezierPath(ovalInRect:
                CGRect(origin: point,
                    size: CGSize(width: 5.0, height: 5.0)))
            circle.fill()
        }
        //Draw horizontal graph lines on the top of everything
        var linePath = UIBezierPath()
        
        //top line
        linePath.moveToPoint(CGPoint(x:margin, y: topBorder))
        linePath.addLineToPoint(CGPoint(x: width - margin,
            y:topBorder))
        
        //center line
        linePath.moveToPoint(CGPoint(x:margin,
            y: graphHeight/2 + topBorder))
        linePath.addLineToPoint(CGPoint(x:width - margin,
            y:graphHeight/2 + topBorder))
        
        //bottom line
        linePath.moveToPoint(CGPoint(x:margin,
            y:height - bottomBorder))
        linePath.addLineToPoint(CGPoint(x:width - margin,
            y:height - bottomBorder))
        let color = UIColor(white: 1.0, alpha: 0.3)
        color.setStroke()
        
        linePath.lineWidth = 1.0
        linePath.stroke()
    }
    func syncStock()
    {   var ind = 0
        if gelados.count>0
        {
            
            var name:String
            for index in 0...icecreams.count-1
            {
                //print("init")
                
                for indexTmp in 0...gelados.count-1
                {
                    name = (gelados[indexTmp].name as? String)!
                    //print(icecreams[index].nome?.caseInsensitiveCompare(name) == NSComparisonResult.OrderedSame)
                    if (icecreams[index].nome?.caseInsensitiveCompare(name) == NSComparisonResult.OrderedSame)
                    {
                        // print(index)
                        //print(name)
                        //print(gelados[indexTmp].vendas)
                        var total = (Double(gelados[indexTmp].vendas)) * (Double(icecreams[index].preco!))
                        graphPoints[index] = Int(total)
                        //labelsNum[ind].text = "\(gelados[indexTmp].vendas)"
                        //ind += 1
                        labelsNum[index].text = "\(total)"
                        
                    }
                    /*else
                     {
                     //print(ind)
                     graphPoints.append(0)
                     //labelsNum[ind].text = "\(0)"
                     //ind += 1
                     }*/
                    
                    
                }
                print(graphPoints.count)
                
                
            }
        }
        
        
    }
}