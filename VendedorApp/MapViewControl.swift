//
//  MapViewControl.swift
//  VendedorApp
//
//  Created by students@deti on 22/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class MapViewControl: ViewController,CLLocationManagerDelegate,MKMapViewDelegate {
   let regionRadius: CLLocationDistance = 1000
    var mylocation:MKMapItem?
    var dest:MKMapItem?
    var wait = 3000
    let locationManager = CLLocationManager()
    let nc = NSNotificationCenter.defaultCenter()
    @IBOutlet weak var mapview: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nc.addObserver(self, selector: #selector(calculateSegmentDirections), name: "calculateSegmentDirections", object: nil)
        
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        
        centerMapOnLocation(initialLocation)
        if #available(iOS 8.0, *) {
            self.locationManager.requestAlwaysAuthorization()
        } else {
            // Fallback on earlier versions
        }
        
        // For use in foreground
        if #available(iOS 8.0, *) {
            self.locationManager.requestWhenInUseAuthorization()
        } else {
            // Fallback on earlier versions
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            if #available(iOS 8.0, *) {
                locationManager.requestAlwaysAuthorization()
            } else {
                // Fallback on earlier versions
            }
            locationManager.startUpdatingLocation()
            
            mapview.showsUserLocation = true
            mapview.zoomEnabled = true
            mapview.scrollEnabled = true
            mapview.pitchEnabled = true
            mapview.delegate = self
            mapview.userInteractionEnabled = true
            mapview.multipleTouchEnabled = true
        }
        // Do any additional setup after loading the view.
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        var locValue:CLLocationCoordinate2D = manager.location!.coordinate
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        var locMark = MKPlacemark(coordinate: CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude), addressDictionary: nil)
        /*var destMark = MKPlacemark(coordinate: CLLocationCoordinate2DMake(next.coordinate.latitude, next.coordinate.longitude), addressDictionary: nil)*/
        
            mylocation = MKMapItem(placemark: locMark)
        
        //destination = MKMapItem(placemark: destMark)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            if self.wait >= 3000
            {
                self.wait = 0
                // Place details
                if let placeMark = placemarks?.first
                {
                    // Rest of your code
            
                    //var placeMark: CLPlacemark!
                    //placeMark = placemarks?[0]
            
                    // Address dictionary
                    //print(placeMark.addressDictionary)
                    //print(placeMark.addressDictionary!["Name"])
                    // Location name
                    if var locationName = placeMark.addressDictionary!["Name"] as? NSString {
                        //print(locationName)
                    }
            
                    // Street address
                    if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                        //print(street)
                    }
            
                    // City
                    if let city = placeMark.addressDictionary!["City"] as? NSString {
                        //print(city)
                        let nc = NSNotificationCenter.defaultCenter()
                        let myDict:NSDictionary = ["cidade":city]
                        nc.postNotificationName("loadDataFromURL", object:myDict  )
                    }
            
                    // Zip code
                    if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                        //print(zip)
                    }
            
                    // Country
                    if let country = placeMark.addressDictionary!["Country"] as? NSString {
                        //print(country)
                    }
                }
                
            }
            self.wait = self.wait + 1
        })
    
        centerMapOnLocation(manager.location!)
    }
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError)
    {
        print("error" + error.localizedDescription)
    }
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapview.setRegion(coordinateRegion, animated: true)
    }
    func calculateSegmentDirections(notification: NSNotification) {
        let getCoord = notification.object as! NSDictionary
        
        var mydest = getCoord["Coord"] as? CLLocation
        var destMark = MKPlacemark(coordinate: CLLocationCoordinate2DMake(mydest!.coordinate.latitude, mydest!.coordinate.longitude), addressDictionary: nil)
        
        dest = MKMapItem(placemark: destMark)

        // 1
        let request: MKDirectionsRequest = MKDirectionsRequest()
        request.source = mylocation
        request.destination = dest
        
        // 2
        request.requestsAlternateRoutes = true
        // 3
        request.transportType = .Automobile
        // 4
        let directions = MKDirections(request: request)
        directions.calculateDirectionsWithCompletionHandler ({
            (response: MKDirectionsResponse?, error: NSError?) in
            if let routeResponse = response?.routes {
                let quickestRouteForSegment: MKRoute =
                    routeResponse.sort({$0.expectedTravelTime <
                        $1.expectedTravelTime})[0]
                self.plotPolyline(quickestRouteForSegment)
            } else if let _ = error {
                //print(error)
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: nil,
                        message: "Directions not available.", preferredStyle: .Alert)
                    let okButton = UIAlertAction(title: "OK",
                    style: .Cancel) { (alert) -> Void in
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                    alert.addAction(okButton)
                    self.presentViewController(alert, animated: true,
                        completion: nil)
                } else {
                    let alert = UIAlertView()
                    alert.title = "Sorry"
                    alert.message = "Directions not available."
                    alert.addButtonWithTitle("Ok")
                    alert.show()
                    // Fallback on earlier versions
                }
               
            }
        })
    }
    func plotPolyline(route: MKRoute) {
        // 1
        var overlays = mapview.overlays
        mapview.removeOverlays(overlays)
       // print(route.polyline)
        mapview.addOverlay(route.polyline,level: MKOverlayLevel.AboveRoads)
        mapview.setVisibleMapRect(route.polyline.boundingMapRect,
                                  edgePadding: UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0),
                                  animated: true)
        
        
       
    }
    func mapView(mapView: MKMapView,
                 rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer! {
        
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if (overlay is MKPolyline) {
            
                polylineRenderer.strokeColor =
                    UIColor.blueColor().colorWithAlphaComponent(0.75)
                        polylineRenderer.lineWidth = 5
        }
        return polylineRenderer
    }
    

    

}
