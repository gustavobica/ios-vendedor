//
//  netview.swift
//  VendedorApp
//
//  Created by students@deti on 25/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//


import UIKit

class netview: UIViewController {

    @IBOutlet weak var netViewer: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        netViewer.loadRequest(NSURLRequest(URL: NSURL(string: "http://www.ola.pt")!))
    }
}
