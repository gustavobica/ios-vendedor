//
//  TableController.swift
//  VendedorApp
//
//  Created by students@deti on 22/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import UIKit

class TableController: UITableViewController {
    
    @IBOutlet var table2: UITableView!
     let URL = "http://api.eventful.com/json/events/search?app_key=vQcJ6N36NQM3dd8J&location=Aveiro";
    var TableData:Array< String > = Array < String >()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        loadDataFromURL(URL)
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadDataFromURL(url:String)
    {
        // Asynchronous Http call to your api url, using NSURLSession:
        NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: url)!, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            if error == nil && data != nil
            {
                print("everyone is fine!")
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                    let json =
                        try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [String:AnyObject]
                    print(json)
                    // Access specific key with value of type String
                    let str = json["page_size"] as! String
                    print("Dani's age is \(str)")
                    let events = json["events"]
                    print("Dani's array is \(events)")
                    /* for (var i = 0; i < events!.count ; i+=1 )
                     {
                     if let event = events![i] as? NSDictionary
                     {
                     if let region_name = event["region_name"] as? String
                     {
                     /*if let country_code = country_obj["code"] as? String
                     {
                     TableData.append(country_name + " [" + country_code + "]")
                     }*/
                     }
                     
                     if let start_time = event["start_time"] as? String
                     {
                     print(start_time)
                     }
                     if let description = event["description"] as? String
                     {
                     print(description)
                     
                     }
                     if let venue_display = event["venue_display"] as? String
                     {
                     print(venue_display)
                     
                     }
                     if let title = event["title"] as? String
                     {
                     print(title)
                     
                     }
                     if let venue_address = event["venue_address"] as? String
                     {
                     print(venue_address)
                     
                     }
                     // TableData.append(title! + " [" + description + "]")
                     
                     
                     }
                     }*/
                    /*let region_name = event!["region_name"] as! String
                     let start_time = event!["start_time"] as! String
                     let description = event!["description"] as! String
                     let venue_display = event!["venue_display"] as! String
                     let title = event!["title"] as! String
                     let venue_address = event!["venue_address"] as! String*/
                    
                    
                } catch {
                    // Something went wrong
                }
            }
        }).resume()
    }


    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return TableData.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) 
        cell.textLabel?.text = TableData[indexPath.row]
        return cell
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
