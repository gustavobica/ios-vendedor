//
//  SaleTotal.swift
//  VendedorApp
//
//  Created by students@deti on 24/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import UIKit
import NotificationCenter


class SaleTotal: UIViewController {
    
    @IBOutlet weak var TotalLabel: UILabel!
    @IBOutlet weak var cCLabel: UILabel!
    @IBOutlet weak var cMLabel: UILabel!
    @IBOutlet weak var cLLabel: UILabel!
    @IBOutlet weak var cNLabel: UILabel!
  
    @IBOutlet weak var mCLabel: UILabel!
    @IBOutlet weak var mBLabel: UILabel!
    @IBOutlet weak var mALabel: UILabel!
    @IBOutlet weak var fLLabel: UILabel!
    
    @IBOutlet weak var pPLabel: UILabel!
    @IBOutlet weak var sLabel: UILabel!
    
    
    
    
    @IBAction func cancelAction(sender: AnyObject) {
        nc.postNotificationName("resetValues", object: nil  )
        ResetValues()
    }
    @IBAction func pagarAction(sender: AnyObject) {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Total pago", message:
                "Foi pago um total de "+"\(total_a_pagar)" + "€", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alert = UIAlertView()
            alert.title = "Total pago"
            alert.message = "Foi pago um total de"+"\(total_a_pagar)" + "€"
            alert.addButtonWithTitle("Ok")
            alert.show()
            // Fallback on earlier versions
        }
        
        nc.postNotificationName("saveBuy", object: nil  )
        ResetValues()
        
    }
    @IBOutlet weak var maxiLabel: UILabel!
   
    var LabelsArray: [UILabel] = []
    let nc = NSNotificationCenter.defaultCenter()
    var total_a_pagar = 0.0
    
        override func viewDidLoad() {
        super.viewDidLoad()
            LabelsArray.append(cCLabel)
            LabelsArray.append(cMLabel)
            LabelsArray.append(cLLabel)
            LabelsArray.append(cNLabel)
            LabelsArray.append(fLLabel)
            LabelsArray.append(mALabel)
            LabelsArray.append(mBLabel)
            LabelsArray.append(mCLabel)
            LabelsArray.append(pPLabel)
            LabelsArray.append(sLabel)
            LabelsArray.append(maxiLabel)
            nc.addObserver(self, selector: #selector(userLoggedIn), name: "UserLoggedIn", object: nil)
            for index in 0...LabelsArray.count-1
            {
                LabelsArray[index].text = "\(0)"
            }
            
        //collectionView.dataSource = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func userLoggedIn(notification: NSNotification){
        
        var adicionou = notification.object as! NSDictionary
        let price = adicionou["preco"] as! Double
        total_a_pagar = total_a_pagar + price
        TotalLabel.text = "\(total_a_pagar)"
        LabelsArray[adicionou["pos"] as! Int].text = "\(Int(LabelsArray[adicionou["pos"] as! Int].text!)! + 1)"
       

    }
    func ResetValues()
    {
        for index in 0...LabelsArray.count-1
        {
            LabelsArray[index].text = "\(0)"
        }
        total_a_pagar = 0
        TotalLabel.text = "\(total_a_pagar)"
        


    }
    
    
   
    
    
}
