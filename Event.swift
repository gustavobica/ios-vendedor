//
//  Event.swift
//  VendedorApp
//
//  Created by students@deti on 23/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import Foundation
import UIKit
import MapKit
struct Event {
    var nome: String?
    var local: String?
    var data:String?
    var img:String?
    var coord:CLLocation?
    
    init(nome: String?, local: String?, data: String?, img:String?, coord:CLLocation?) {
        self.nome = nome
        self.local = local
        self.data = data
        self.img = img
        self.coord = coord
    }
    
}
