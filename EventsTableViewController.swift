//
//  EventsTableViewController.swift
//  VendedorApp
//
//  Created by students@deti on 23/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import UIKit
import MapKit

class EventsTableViewController: UITableViewController {
    
    let URL = "http://api.eventful.com/json/events/search?app_key=vQcJ6N36NQM3dd8J&location=";
    var Gelados:[Gelado]=[]
    let URLStock = "http://www.gustavobica.com/CM/API.php?action=CarStock&id="+"\(mainInstance.id)";
    var events:[Event]=[]
    let nc = NSNotificationCenter.defaultCenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nc.addObserver(self, selector: #selector(loadDataFromURL), name: "loadDataFromURL", object: nil)
        if(mainInstance.id>0)
        {
            loadStockFromURL(URLStock)
            
        }
            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return events.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) //1
        
        let event = events[indexPath.row] as Event //2
        
        if let nomeLabel = cell.viewWithTag(100) as? UILabel { //3
            nomeLabel.text = event.nome
        }
        if let localLabel = cell.viewWithTag(101) as? UILabel {
            localLabel.text = event.local
        }
        if let dataLabel = cell.viewWithTag(102) as? UILabel {
            dataLabel.text = event.data
        }
        if let imgView = cell.viewWithTag(103) as? UIImageView {
            imgView.image = self.imageForRating(event.img!)
        }
        return cell
    }
    func imageForRating(img:String) -> UIImage? {
       
        if let url = NSURL(string: img) {
            if let data = NSData(contentsOfURL: url) {
                return UIImage(data: data)
            }        
        }
        return UIImage(named: "event")
    }
    func do_table_refresh()
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
            return
        })
    }
    
    func loadDataFromURL(notification: NSNotification)
    {
        let urlcidade = notification.object as! NSDictionary
        let urlCidade = urlcidade["cidade"] as! String
        //print(urlCidade)
        var url = "\(URL + urlCidade)"
        url = url.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: NSStringCompareOptions.LiteralSearch, range: nil)
        //print(url)
        // Asynchronous Http call to your api url, using NSURLSession:
        NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: url)!, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            if error == nil && data != nil
            {
          
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                    let json =
                        try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [String:AnyObject]
                    // Access specific key with value of type String
                   
                    self.events.removeAll()
                    let eventos = json["events"]
                    let eventday = eventos!["event"]
                    
                   
                    for index in 0...eventday!!.count-1 {
                        var ev=eventday!![index]
                        var imgUrl=ev["image"]
                        //print(imgUrl)
                        if let smallUrl = imgUrl as? [String: AnyObject] {
                            //imgUrl = ev["small"]!!["url"]
                            if let url = smallUrl["small"]  as? [String: AnyObject]
                            {
                                if let final_url = url["url"]  as? String
                                {
                                    imgUrl = final_url
                                }
                                else
                                {
                                    imgUrl = "none"
                                }

                            }
                            else
                            {
                                imgUrl = "none"
                            }
                            
                            
                        }else
                        {
                            
                            imgUrl = "none"
                            
                        }
                        
                        var latitude = (ev["latitude"]as! NSString).doubleValue as! Double
                        
                        var longitude = (ev["longitude"]as! NSString).doubleValue as! Double
                       var loc = CLLocation(latitude: latitude , longitude: longitude )
                        self.events.append(Event(nome:ev["title"] as! String,local:ev["region_name"] as! String,data:ev["start_time"] as! String,img:imgUrl as! String,coord:loc))
                        
                    }
                    
                    self.do_table_refresh()
                    
                    
                    
                } catch {
                    // Something went wrong
                }
            }
        }).resume()
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let nc = NSNotificationCenter.defaultCenter()
        //print(events[indexPath.row].coord)
        let myDict:NSDictionary = ["Coord":events[indexPath.row].coord!]
        nc.postNotificationName("calculateSegmentDirections", object:myDict  )
        //print(indexPath.row)
        
    }
    
    func saveStock()
    {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(Gelados, toFile: Gelado.ArchiveURL.path!)
        if !isSuccessfulSave {
            print("Failed to save Gelados...")
        }
    }
    func loadGelados() -> [Gelado]? {
        return NSKeyedUnarchiver.unarchiveObjectWithFile(Gelado.ArchiveURL.path!) as? [Gelado]
    }
    func loadStockFromURL(url: String)
    {
        
        //print("entrei")
        // Asynchronous Http call to your api url, using NSURLSession:
        NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: url)!, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            //print(data)
            if error == nil && data != nil
            {
                //print(data)
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                    let json =
                        try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [String:AnyObject]
                    // Access specific key with value of type String
                    
                    //self.carrosID.removeAll()
                    var id:String
                    
                    var list=json["stockList"]
                    for index in 0...list!.count-1
                    {
                        var name = list![index]["iceName"] as! String
                        
                        var quant = list![index]["quantity"] as! Int
                        var b = Gelado(name: name,quant: quant, vendas: 0)
                        self.Gelados.append(b!)
                        self.saveStock()
                        
                        //Gelados.append(Gelado(coder: a))
                        //print(json[index])
                        //var id = json[index]["carId"] as? Int
                        //id = id["carId"] as? String
                        //self.carrosID.append(id!)
                    }
                    
                    
                } catch {
                    //print("aqui")
                    //print(data)
                    // Something went wrong
                }
            }
        }).resume()
    }
    
}


