//
//  ScannerViewController.swift
//  VendedorApp
//
//  Created by students@deti on 26/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import AVFoundation
import UIKit

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
   var gelados:[Gelado] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
         var json2 = "[{\"name\":\"Cornetto Morango \", \"quant\":1},{\"name\":\"Cornetto Chocolate \", \"quant\":1}]"
        confirm(json2);
        //captureQRCode()
        
    }
    func confirm(json2:String)
    {
        
        //print(json2)
        let json: AnyObject? = json2.parseJSONString
        //print("Parsed JSON: \(json!)")
        //print(json![0]["name"])
        //print("json[3]: \(json[0] as String)")
        //Do parse
        gelados = loadGelados()!
        var Clientrequest = ""
        var name:String
        var name2:String
        for index in 0...gelados.count-1
        {
            for ind in 0...json!.count-1
            {
                name = (gelados[index].name).uppercaseString
                name2 = (json![0]["name"] as! String).uppercaseString
                print(name)
                print(name2)
                
                print(name.caseInsensitiveCompare(json![0]["name"]!! as! String ) == NSComparisonResult.OrderedSame )
                if (name.caseInsensitiveCompare(json![0]["name"]!! as! String ) == NSComparisonResult.OrderedSame)
                {
                    gelados[index].vendas = (gelados[index].vendas ) + (json![ind]["quant"]!! as! Int)
                    gelados[index].quant = (gelados[index].quant ) + (json![ind]["quant"]!! as! Int)
                    
                }
            }
            
        }
        saveStock()
        for ind in 0...json!.count-1
        {
            Clientrequest = Clientrequest + "\n"+((json![ind]["name"]!!) as! String) + " Quantidade:" + "\(json![ind]["quant"]!!)"
        }
        var texto = "O QrCode lido é valido, lista de gelados a fornecer ao cliente: " + Clientrequest
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Pedido Aceite", message:
                texto, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil)) //{ action in self.performSegueWithIdentifier("ScannerOkay", sender: self) }))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alert = UIAlertView()
            alert.title = "Pedido aceite"
            alert.message = "O QrCode lido é valido, lista de gelados a fornecer ao cliente: " + Clientrequest
            alert.addButtonWithTitle("Ok")
            alert.show()
            // Fallback on earlier versions
        }
        
    }
    func captureQRCode() {
        let session = AVCaptureSession()
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        do {
            let input = try AVCaptureDeviceInput(device: device) as AVCaptureDeviceInput
        session.addInput(input)
        
        let output = AVCaptureMetadataOutput()
        output.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        session.addOutput(output)
        output.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        let bounds = self.view.layer.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer.bounds = bounds
        previewLayer.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds))
        
        self.view.layer.addSublayer(previewLayer)
        session.startRunning()
        }catch let error as NSError {print(error)}
    }
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        for item in metadataObjects {
            if let metadataObject = item as? AVMetadataMachineReadableCodeObject {
                if metadataObject.type == AVMetadataObjectTypeQRCode {
                    print("QR Code: \(metadataObject.stringValue)")
                   
                    confirm(metadataObject.stringValue)
                }
            }
        }
    }
    func saveStock()
    {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(gelados, toFile: Gelado.ArchiveURL.path!)
        if !isSuccessfulSave {
            print("Failed to save Gelados...")
        }
    }
    func loadGelados() -> [Gelado]? {
        return NSKeyedUnarchiver.unarchiveObjectWithFile(Gelado.ArchiveURL.path!) as? [Gelado]
    }
}
extension String {
    
    var parseJSONString: AnyObject? {
        
        let data = self.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do{
                let r = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)
                return r
            }catch let error as NSError
            {
                print(error)
                    return error
            }
            
            
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
}