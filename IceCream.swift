//
//  IceCream.swift
//  VendedorApp
//
//  Created by students@deti on 23/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//

import Foundation
import UIKit

struct IceCream {
    var nome: String?
    var quant: Int?
    var preco:Double?
    var img:String?
    
    init(nome: String?, quant: Int?, preco: Double?, img:String?) {
        self.nome = nome
        self.quant = quant
        self.preco = preco
        self.img = img
    }
}

