//
//  Gelado.swift
//  VendedorApp
//
//  Created by students@deti on 25/06/16.
//  Copyright © 2016 students@deti. All rights reserved.
//
import UIKit

class Gelado: NSObject, NSCoding {
    var name:String
    var vendas:Int
    /*var photo:String
    var rating:Int*/
    var quant:Int
    init?(name: String,/* photo: String, rating: Int,*/ quant: Int, vendas: Int) {
        // Initialize stored properties.
        self.name = name
        /*self.photo = photo
        self.rating = rating*/
        self.quant = quant
        self.vendas = vendas
        super.init()
        
        // Initialization should fail if there is no name or if the rating is negative.
        /*if name.isEmpty || rating < 0 {
            return nil
        }*/
        if name.isEmpty || quant < 0 {
            return nil
        }
    }
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: PropertyKey.nameKey)
       /* aCoder.encodeObject(photo, forKey: PropertyKey.photoKey)
        aCoder.encodeInteger(rating, forKey: PropertyKey.ratingKey)*/
         aCoder.encodeInteger(quant, forKey: PropertyKey.quantKey)
        aCoder.encodeInteger(vendas, forKey: PropertyKey.vendasKey)
    }
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObjectForKey(PropertyKey.nameKey) as! String
      /*
        // Because photo is an optional property of Meal, use conditional cast.
        let photo = aDecoder.decodeObjectForKey(PropertyKey.photoKey) as! String
        
        let rating = aDecoder.decodeIntegerForKey(PropertyKey.ratingKey)*/
        
        let quant = aDecoder.decodeIntegerForKey(PropertyKey.quantKey)
        let vendas = aDecoder.decodeIntegerForKey(PropertyKey.vendasKey)
        
        // Must call designated initializer.
        self.init(name: name/*, photo: photo, rating: rating*/, quant: quant, vendas: vendas)
    }
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("gelados")
}
struct PropertyKey {
    static let nameKey = "name"
    static let vendasKey = "vendas"
    //static let photoKey = "photo"
    //static let ratingKey = "rating"
    static let quantKey = "quantidade"
}